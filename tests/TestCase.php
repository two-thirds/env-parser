<?php

namespace TwoThirds\Testing;

use PHPUnit\Framework\TestCase as BaseTestCase;

class TestCase extends BaseTestCase
{
    /**
     * Create a temporary env file
     *
     * @param array|string $contents
     *
     * @return string
     */
    public function createEnvFile($contents)
    {
        if (is_array($contents)) {
            $contents = implode($contents, "\n");
        }

        $filename = tempnam(sys_get_temp_dir(), 'EnvParser');

        file_put_contents($filename, $contents);

        return $filename;
    }
}
