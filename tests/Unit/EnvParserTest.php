<?php

namespace TwoThirds\Testing\Unit;

use TwoThirds\Testing\TestCase;
use TwoThirds\EnvParser\EnvParser;
use Dotenv\Exception\InvalidFileException;
use Dotenv\Exception\InvalidPathException;

/**
 * @SuppressWarnings(PHPMD.TooManyPublicMethods)
 */
class EnvParserTest extends TestCase
{
    /**
     * @test
     */
    public function parserThrowsExceptionWhenFileNotFound()
    {
        $this->expectException(InvalidPathException::class);

        EnvParser::parse(__DIR__ . 'file_that_doesnt_exist.env');
    }

    /**
     * @test
     */
    public function dotenvLoadsEnvironmentVars()
    {
        $file = $this->createEnvFile([
            'FOO=bar',
            'BAR=baz',
            'SPACED="with spaces"',
            '',
            'NULL=',
            '',
        ]);

        $parsed = EnvParser::parse($file);

        $this->assertEquals('bar', $parsed['FOO']);
        $this->assertEquals('baz', $parsed['BAR']);
        $this->assertEquals('with spaces', $parsed['SPACED']);
        $this->assertEmpty($parsed['NULL']);
    }

    /**
     * @test
     */
    public function doesNotImportVariablesFromEnvironmentByDefault()
    {
        putenv('FOOBAR=BAZQUX');
        $this->assertEquals('BAZQUX', getenv('FOOBAR'));

        $file = $this->createEnvFile([
            'BARBAZ=${FOOBAR}',
        ]);

        $parsed = EnvParser::parse($file);

        $this->assertEquals('${FOOBAR}', $parsed['BARBAZ']);
    }

    /**
     * @test
     */
    public function canImportVariablesFromEnv()
    {
        putenv('FOOBAR=BAZQUX');
        $this->assertEquals('BAZQUX', getenv('FOOBAR'));

        $file = $this->createEnvFile([
            'BARBAZ=${FOOBAR}',
        ]);

        $parsed = EnvParser::parse($file, false);

        $this->assertEquals('BAZQUX', $parsed['BARBAZ']);
    }

    /**
     * @test
     */
    public function commentedDotenvLoadsEnvironmentVars()
    {
        $file = $this->createEnvFile([
            '# This is a comment',
            'CFOO=bar',
            '#CBAR=baz',
            '#CZOO=goo # a comment on a commented row',
            'CSPACED="with spaces" # this is a comment',
            'CQUOTES="a value with a # character" # this is a comment',
            'CQUOTESWITHQUOTE="a value with a # character & a quote \" character inside quotes" # " this is a comment',
            'EMPTY= # comment with empty variable',
            'BOOLEAN=yes # (yes, no)',
            '',
            'CNULL=',
            '',
            '## this is a comment ##',
        ]);

        $parsed = EnvParser::parse($file);

        $this->assertSame('bar', $parsed['CFOO']);
        $this->assertFalse(isset($parsed['CBAR']));
        $this->assertFalse(isset($parsed['CZOO']));
        $this->assertSame('with spaces', $parsed['CSPACED']);
        $this->assertSame('a value with a # character', $parsed['CQUOTES']);
        $this->assertSame('a value with a # character & a quote " character inside quotes', $parsed['CQUOTESWITHQUOTE']);
        $this->assertEmpty($parsed['CNULL']);
        $this->assertEmpty($parsed['EMPTY']);
    }

    /**
     * @test
     */
    public function quotedDotenvLoadsEnvironmentVars()
    {
        $file = $this->createEnvFile([
            'QFOO="bar"',
            'QBAR="baz"',
            'QSPACED="with spaces"',
            'QEQUALS="pgsql:host=localhost;dbname=test"',
            '',
            'QNULL=""',
            'QWHITESPACE = "no space"',
            '',
            'QESCAPED="test some escaped characters like a quote (\\") or maybe a backslash (\\\\)"',
            'QSLASH="iiiiviiiixiiiiviiii\\\\n"',
            '',
            "SQESCAPED='test some escaped characters like a quote (\\') or maybe a backslash (\\\\)'",
            "SQSLASH='iiiiviiiixiiiiviiii\\\\n'",
        ]);

        $parsed = EnvParser::parse($file);

        $this->assertSame('bar', $parsed['QFOO']);
        $this->assertSame('baz', $parsed['QBAR']);
        $this->assertSame('with spaces', $parsed['QSPACED']);
        $this->assertEmpty($parsed['QNULL']);
        $this->assertSame('pgsql:host=localhost;dbname=test', $parsed['QEQUALS']);
        $this->assertSame('test some escaped characters like a quote (") or maybe a backslash (\\)', $parsed['QESCAPED']);
        $this->assertSame('iiiiviiiixiiiiviiii\\n', $parsed['QSLASH']);
        $this->assertSame('test some escaped characters like a quote (\') or maybe a backslash (\\)', $parsed['SQESCAPED']);
        $this->assertSame('iiiiviiiixiiiiviiii\\n', $parsed['SQSLASH']);
    }


    /**
     * @test
     */
    public function largeDotenvLoadsEnvironmentVars()
    {
        $file = $this->createEnvFile([
            'LARGE=' . str_repeat('1', 3000),
        ]);

        $parsed = EnvParser::parse($file);

        $this->assertNotEmpty($parsed['LARGE']);
        $this->assertEquals(strlen($parsed['LARGE']), 3000);
    }

    /**
     * @test
     */
    public function spacedValuesWithoutQuotesThrowsException()
    {
        $this->expectException(InvalidFileException::class);

        $file = $this->createEnvFile([
            'QWFOO=with spaces',
        ]);

        EnvParser::parse($file);
    }

    /**
     * @test
     */
    public function exportedDotenvLoadsEnvironmentVars()
    {
        $file = $this->createEnvFile([
            'export EFOO="bar"',
            'export EBAR="baz"',
            'export ESPACED="with spaces"',
            '',
            'export ENULL=""',
        ]);

        $parsed = EnvParser::parse($file);

        $this->assertSame('bar', $parsed['EFOO']);
        $this->assertSame('baz', $parsed['EBAR']);
        $this->assertSame('with spaces', $parsed['ESPACED']);
        $this->assertEmpty($parsed['ENULL']);
    }

    /**
     * @test
     */
    public function dotenvNestedEnvironmentVars()
    {
        $file = $this->createEnvFile([
            'NVAR1="Hello"',
            'NVAR2="World!"',
            'NVAR3="{$NVAR1} {$NVAR2}"',
            'NVAR4="${NVAR1} ${NVAR2}"',
            'NVAR5="$NVAR1 {NVAR2}"',
            'N.VAR6="Special Value"',
            'NVAR7="${N.VAR6}"',
            'NVAR8=""',
            'NVAR9="${NVAR8}"',
            'NVAR10="${NVAR888}"',
        ]);

        $parsed = EnvParser::parse($file);

        $this->assertSame('{$NVAR1} {$NVAR2}', $parsed['NVAR3']); // not resolved
        $this->assertSame('Hello World!', $parsed['NVAR4']);
        $this->assertSame('$NVAR1 {NVAR2}', $parsed['NVAR5']); // not resolved
        $this->assertSame('Special Value', $parsed['N.VAR6']); // new '.' (dot) in var name
        $this->assertSame('Special Value', $parsed['NVAR7']);  // nested '.' (dot) variable
        $this->assertSame('', $parsed['NVAR8']);
        $this->assertSame('', $parsed['NVAR9']);  // nested variable is empty string
        $this->assertSame('${NVAR888}', $parsed['NVAR10']);  // nested variable is not set
    }

    /**
     * @test
     */
    public function dotenvTrimmedKeys()
    {
        $file = $this->createEnvFile([
            'QWHITESPACE = "no space"',
        ]);

        $parsed = EnvParser::parse($file);

        $this->assertSame('no space', $parsed['QWHITESPACE']);
    }

    /**
     * @test
     */
    public function dotenvAllowsSpecialCharacters()
    {
        $file = $this->createEnvFile([
            'SPVAR1="$a6^C7k%zs+e^.jvjXk"',
            'SPVAR2="?BUty3koaV3%GA*hMAwH}B"',
            'SPVAR3="jdgEB4{QgEC]HL))&GcXxokB+wqoN+j>xkV7K?m$r"',
            'SPVAR4="22222:22#2^{"',
            'SPVAR5="test some escaped characters like a quote \" or maybe a backslash \\\\" # not escaped',
            'SPVAR6=secret!@#',
            "SPVAR7='secret!@#'",
            'SPVAR8="secret!@#"',
        ]);

        $parsed = EnvParser::parse($file);

        $this->assertSame('$a6^C7k%zs+e^.jvjXk', $parsed['SPVAR1']);
        $this->assertSame('?BUty3koaV3%GA*hMAwH}B', $parsed['SPVAR2']);
        $this->assertSame('jdgEB4{QgEC]HL))&GcXxokB+wqoN+j>xkV7K?m$r', $parsed['SPVAR3']);
        $this->assertSame('22222:22#2^{', $parsed['SPVAR4']);
        $this->assertSame('test some escaped characters like a quote " or maybe a backslash \\', $parsed['SPVAR5']);
        $this->assertSame('secret!@', $parsed['SPVAR6']);
        $this->assertSame('secret!@#', $parsed['SPVAR7']);
        $this->assertSame('secret!@#', $parsed['SPVAR8']);
    }
}
