# Dot Env Parser

[![pipeline status](https://gitlab.com/two-thirds/env-parser/badges/master/pipeline.svg)](https://gitlab.com/two-thirds/env-parser/commits/master)
[![coverage report](https://gitlab.com/two-thirds/env-parser/badges/master/coverage.svg)](https://gitlab.com/two-thirds/env-parser/commits/master)

This is a simple library that extends `vlucas/phpdotenv` to parse an env file and return an array of values. It is designed to be completely isolated from the current environment by default. It will never export variables into the current environment. By default it will not import current environment variables for nested replacement, but there is an option to do so.

`.env.test`:
```
EXAMPLE="Hello World!"
NUMBER=123
WITH_SPACES="with spaces"
REPLACEMENT="the example variable says ${EXAMPLE}"
```

```php
$variables = EnvParser::parse(realpath('.env.test'));
```

```php
$variables === [
    'EXAMPLE' => 'Hello World!',
    'NUMBER' => '123',
    'WITH_SPACES' => 'with spaces',
    'REPLACEMENT' => 'the example variable says Hello World!',
];
```

> **Note:** The returned values will always be strings. In certain frameworks like Laravel, conversion of `"(false)"` to `false` or `"null"` to `null` happens within the framework and is not the responsibility of this library or `vlucas/phpdotenv`

## Versions

This library currently supports two versions of `vlucas/phpdotenv`

| env-parser tag | phpdotenv tag |
| -- | -- |
| 2.2.x | ^2.2 |
| 3.3.x | ^3.3 |

This is primarily done to provide support for Laravel 5.3 and up, which at the time of writing this only used ^2.2 and ^3.3. If you encounter bugs, please be sure to report the exact version of phpdotenv that you're using so this can be adjusted accordingly.
