<?php

namespace TwoThirds\EnvParser;

use Dotenv\Loader;
use Dotenv\Environment\DotenvFactory;
use TwoThirds\EnvParser\Adapters\GetenvAdapter;

class EnvParser extends Loader
{
    /**
     * The array of parsed data
     *
     * @var array
     */
    protected $parsed = [];

    /**
     * Prevent parsing from getting values from the current environment
     *
     * @var boolean
     */
    protected $isolated = true;

    /**
     * Parse the provided filename
     *
     * @param string $filename
     * @param boolean $isolated
     *
     * @return array
     *
     * @SuppressWarnings(PHPMD.BooleanArgumentFlag)
     */
    public static function parse(string $filename, bool $isolated = true)
    {
        $adapters = [];

        if (! $isolated) {
            $adapters[] = new GetenvAdapter;
        }

        $factory = new DotenvFactory($adapters);

        return (new static([$filename], $factory))
            ->setIsolated($isolated)
            ->load();
    }

    /**
     * Sets isolated in the current instance
     *
     * @param bool $isolated
     */
    protected function setIsolated(bool $isolated)
    {
        $this->isolated = $isolated;

        return $this;
    }

    /**
     * Set an environment variable.
     *
     * @param string      $name
     * @param string|null $value
     *
     * @return void
     */
    public function setEnvironmentVariable($name, $value = null)
    {
        $this->parsed[$name] = $value;
        parent::setEnvironmentVariable($name, $value);
    }

    /**
     * Search the parsed array for the requested variable
     *
     * @param string $name
     *
     * @return string|null
     */
    public function getEnvironmentVariable($name)
    {
        if (! isset($this->parsed[$name]) && ! $this->isolated) {
            return parent::getEnvironmentVariable($name);
        }

        return $this->parsed[$name] ?? null;
    }
}
