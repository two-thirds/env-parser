<?php

namespace TwoThirds\EnvParser\Adapters;

use Dotenv\Environment\Adapter\PutenvAdapter;

class GetenvAdapter extends PutenvAdapter
{
    /**
     * Determines if the adapter is supported.
     *
     * @return bool
     */
    public function isSupported()
    {
        return function_exists('getenv');
    }

    /**
     * Set an environment variable.
     *
     * @param string      $name
     * @param string|null $value
     *
     * @return void
     */
    public function set($name, $value = null)
    {
        // do nothing
    }

    /**
     * Clear an environment variable.
     *
     * @param string $name
     *
     * @return void
     */
    public function clear($name)
    {
        // do nothing
    }
}
